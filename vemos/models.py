kfrom django.db import models


class Server(models.Model):
    name = models.CharField(max_length=40, unique=True)
    description = models.TextField(blank=True)
    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.name


class Type(models.Model):
    name = models.CharField(max_length=20, unique=True)
    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.name


class Service(models.Model):
    server = models.ForeignKey(Server)
    type = models.ForeignKey(Type)
    is_active = models.BooleanField()
    validation_regex_test = models.CharField(max_length=200)
    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return '%s (%s)' % (self.server.name, self.type.name)
      
class Job(models.Model):
  name = models.CharField(max_length=40)
  command = models.CharField(max_length=10, blank=True)

class Log(model.Model):
  task = models.ForeignKey('Task', related_name='task')
  date = models.DateTimeField(auto_now_add=True)
  stdout = models.TextField(blank=True)
  stderr = models.TextField(blank=True)
  result = models.BooleanField(default=True)
  
  def __unicode__(self):
    return '%s (%s)' % (self.job.name, self.date)
